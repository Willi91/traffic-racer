package model;

public class Player {

    private int x;
    private int y;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int dx, int dy) {
        if (this.x == 250 && dx == -150) {
            return;
        } else if (this.x == 700 && dx == 150) {
            return;
        } else {
            this.x += dx;
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 100;
    }

    public int getW() {
        return 50;
    }
}
