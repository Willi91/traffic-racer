package model;

import java.util.Random;

public class Car {

    private int x;
    private int y;
    private float speedY;


    public Car(int y, float speedY) {
        Random random = new Random();
        float zufall = random.nextFloat();

        if ((zufall < 0.25)) {
            this.x = 250;
        } else if (zufall >= 0.25 && zufall < 0.5) {
            this.x = 400;
        } else if (zufall >= 0.5 && zufall < 0.75) {
            this.x = 550;
        } else if (zufall > 0.75) {
            this.x = 700;
        }
        this.y = y;
        this.speedY = speedY;
    }

    public void update(long elapsedTime) {
        this.y = Math.round(this.y + elapsedTime * this.speedY);
    }


    public int getX() {
        return this.x + 25;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return 100;
    }

    public int getW() {
        return 50;
    }


}
