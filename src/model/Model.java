package model;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    public static final int WIDTH = 1000;
    public static final int HEIGHT = 1000;
    public static int score = 0;
    public boolean spielVorbei = false;
    private double nextCarDely;

    private Random random = new Random();

    private List<Car> cars = new LinkedList<>();
    private Player player;

    public Model() {
        this.player = new Player(400, 800);
        carGenerator();
    }

    public void carGenerator() {
        this.cars.add(new Car(0, 0.4f));
        this.nextCarDely = random.nextInt(400) + 400;
    }

    public List<Car> getCars() {
        return cars;
    }

    public Player getPlayer() {
        return player;
    }

    public void restartGame() {
        cars.clear();
        score = 0;
        this.cars.add(new Car(0, 0.4f));
        this.cars.add(new Car(0, 0.4f));
        spielVorbei = false;
        this.nextCarDely = random.nextInt(400) + 400;
    }

    public void update(long elapsedTime) {

        if (!spielVorbei) {

            for (Car car : cars) {
                car.update(elapsedTime);
            }

            nextCarDely -= elapsedTime;
            if (nextCarDely <= 0) {
                carGenerator();
                score++;
            }

            for (Car car : cars) {

                int dx = Math.abs(player.getX() - car.getX());
                int dy = Math.abs(player.getY() - car.getY());
                int w = player.getW() / 2 + car.getW() / 2;
                int h = player.getH() / 2 + car.getH() / 2;

                if (dx <= w && dy <= h) {
                    spielVorbei = true;
                }


            }

        }


    }


}
