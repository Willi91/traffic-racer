import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;
    public boolean restart = false;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.LEFT) {
            model.getPlayer().move(-150, 0);
        } else if (key == KeyCode.RIGHT) {
            model.getPlayer().move(150, 0);
        } else if (key == KeyCode.SPACE) {
            model.restartGame();
        }
    }


    public Model getModel() {
        return model;
    }
}
